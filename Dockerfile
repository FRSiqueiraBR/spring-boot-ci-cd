FROM openjdk:11
COPY target/spring-boot-ci-cd-0.0.1.jar spring-boot-ci-cd-0.0.1.jar
ENTRYPOINT ["java","-jar","/spring-boot-ci-cd-0.0.1.jar"]