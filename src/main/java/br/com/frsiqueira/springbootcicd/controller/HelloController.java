package br.com.frsiqueira.springbootcicd.controller;

import br.com.frsiqueira.springbootcicd.dto.MessageDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/hello")
public class HelloController {

    @RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MessageDTO> helloWorld(){
        return new ResponseEntity<>(new MessageDTO("Hello World"), HttpStatus.OK);
    }
}
